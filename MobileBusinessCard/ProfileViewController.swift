//
//  ProfileViewController.swift
//  MobileBusinessCard
//
//  Created by cookie monster on 09/04/2016.
//  Copyright © 2016 cookie monster. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var ProfilePicture: UIImageView!
    
    
    @IBOutlet weak var ProfileName: UITextField!
    
    @IBOutlet weak var ProfilePosition: UITextField!
    
    
    @IBOutlet weak var ProfileCompany: UITextField!
    
    @IBOutlet weak var ProfileAddress: UITextField!
    
    @IBOutlet weak var ProfileAddress2: UITextField!
    
    @IBOutlet weak var ProfilePhone: UITextField!
    
    @IBOutlet weak var ProfileEmail: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.ProfilePicture.layer.cornerRadius = 54
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ProfileViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 200, right: 0)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        print(textField.text)
        textField.resignFirstResponder()
        return true
    }

}



